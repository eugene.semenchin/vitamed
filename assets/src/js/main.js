// Затемение экрана//
const overlay = document.querySelector('.overlay')


// Бургер Меню // 
const burgerButton = document.querySelector(".burger__button");
const burgerMenu = document.querySelector(".navigation-mobile__block");

burgerButton.addEventListener('click', () => {
	burgerMenu.classList.toggle("navigation-mobile__block--active");
})




// Валидация формы //
;(function() {
	'use strict';

	class Form {
		static patternName = /^[а-яёА-ЯЁ\s]+$/;
    	static patternPhone = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
		static errorMess = [
			'Введите имя', // 0
			'Неверное имя', // 1
      		'Введите телефон', // 2
      		'Неверный телефон', // 3
		];

		constructor(form) {
			this.form = form;
			this.fields = this.form.querySelectorAll('.form-control');
			this.btn = this.form.querySelector('[type=submit]');
			this.iserror = false;
			this.registerEventsHandler();
		}

		static getElement(el) {
			return el.parentElement.nextElementSibling;
		}

		registerEventsHandler() {
			this.btn.addEventListener('click', this.validForm.bind(this));
			this.form.addEventListener('focus', () => {
				const el = document.activeElement;
				if (el === this.btn) return;
				this.cleanError(el);
			}, true);
			for (let field of this.fields) {
				field.addEventListener('blur', this.validBlurField.bind(this));
			}
		}

		validForm(e) {
			e.preventDefault();
			const formData = new FormData(this.form);
			let error;

			for (let property of formData.keys()) {
				error = this.getError(formData, property);
				if (error.length == 0) continue;
				this.iserror = true;
				this.showError(property, error);
			}

			if (this.iserror) return;
			this.sendFormData(formData);
		}

		validBlurField(e) {
			const target = e.target;
			const property = target.getAttribute('name');
			const value = target.value;
			const formData = new FormData();
			formData.append(property, value);
			const error = this.getError(formData, property);
			if (error.length == 0) return;
			this.showError(property, error);
		}

		getError(formData, property) {
			let error = '';
			const validate = {
				username: () => {
          			if (formData.get('username').length == 0) {
						error = Form.errorMess[0];
					} else if (Form.patternName.test(formData.get('username')) == false) {
						error = Form.errorMess[1];
					}
				},
				
				userphone: () => {
					if (formData.get('userphone').length == 0) {
						error = Form.errorMess[2];
					} else if (Form.patternPhone.test(formData.get('userphone')) == false) {
						error = Form.errorMess[3];
					}
				},
			}

			if (property in validate) {
				validate[property]();
			}
			return error;
		}

		showError(property, error) {
			const el = this.form.querySelector(`[name=${property}]`);
			const errorBox = Form.getElement(el);
			el.classList.add('form-control_error');
			errorBox.innerHTML = error;
			errorBox.style.display = 'block';
		}

		cleanError(el) {
			const errorBox = Form.getElement(el);
			el.classList.remove('form-control_error');
			errorBox.removeAttribute('style');
			this.iserror = false;
		}

		sendFormData(formData) {
			let xhr = new XMLHttpRequest();
			const form = document.querySelector('.form')
            const formButton = document.querySelector('.form__button')
            const formSent = document.querySelector('.sent__form')
            const formClose = document.querySelector('.sent__close')
			
			xhr.open('POST', '/sendmail.php', true);
			xhr.onreadystatechange = () => {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об успешной отправке письма
            
					} else {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об ошибке
           
					}

                    } else {
                        // здесь расположен код вашей callback-функции
                        // например, она может выводить сообщение об ошибке
                    }
                }

			// отправляем данные формы
			// xhr.send(formData);
			formButton.addEventListener('click', () => {
                form.classList.toggle('form--hide');
                formSent.classList.toggle('sent__form--show');
            });

            formClose.addEventListener('click', () => {
                form.classList.remove('form--hide');
                formSent.classList.remove('sent__form--show');
            })
		};
	}

	
	const forms = document.querySelectorAll('[name=feedback]');
	if (!forms) return;
	for (let form of forms) {
		const f = new Form(form);
	}
})();

// Слайдер Service Prices // 

$(window).on('load resize', function() {
	if ($(window).width() < 867) {
	  $('.product-cards__slider:not(.slick-initialized)').slick({
		// centerMode: true,
		variableWidth: true,
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 2,
		arrows: false
	  });
	} else {
	  $(".product-cards__slider.slick-initialized").slick("unslick");
	}
});


// Слайдер Other Service Prices // 
$('.product-other-cards__slider').on('init reInit',function(event,slick){
	var amount = slick.slideCount;
	$('.range__service-prices').attr('max',amount);
})
  
$('.product-other-cards__slider').on('afterChange',function(e,slick,currentSlide){
	$('.range__service-prices').val(currentSlide+1);
})
  
$('.range__service-prices').on('input change',function(){
	$('.product-other-cards__slider').slick('slickGoTo',this.value-1);
});

$('.product-other-cards__slider').slick({
	// centerMode: true,
	variableWidth: true,
	dots: false,
	infinite: true,
	speed: 300,
	slidesToShow: 2,
	arrows: true,
	prevArrow: $('.product-other-arrow__prev'),
    nextArrow: $('.product-other-arrow__next'),
	responsive: [
		{
		  breakpoint: 992,
		  settings: {
			variableWidth: true,
			arrows: false,
			slidesToShow: 1,
		  }
		},
	]
});


// Cлайдер Our Specialists Slider // 
$('.our-specialists__slider').on('init reInit',function(event,slick){
	var amount = slick.slideCount;
	$('.range__our-specialists').attr('max',amount);
})
  
$('.our-specialists__slider').on('afterChange',function(e,slick,currentSlide){
	$('.range__our-specialists').val(currentSlide+1);
})
  
$('.range__our-specialists').on('input change',function(){
	$('.our-specialists__slider').slick('slickGoTo',this.value-1);
});

$('.our-specialists__slider').slick({
	// centerMode: true,
	variableWidth: true,
	dots: false,
	infinite: true,
	speed: 300,
	slidesToShow: 2,
	arrows: true,
	prevArrow: $('.our-specialists-arrow__prev'),
    nextArrow: $('.our-specialists-arrow__next'),
	responsive: [
		{
		  breakpoint: 992,
		  settings: {
			variableWidth: true,
			arrows: false,
			slidesToShow: 1,
		  }
		},
	]
});

// Cлайдер Clinic Photo // 
$('.clinic-photo__slider').slick({
	// centerMode: true,
	variableWidth: true,
	dots: false,
	infinite: true,
	speed: 300,
	slidesToShow: 1,
	arrows: true,
	prevArrow: $('.clinic-photo__prev'),
    nextArrow: $('.clinic-photo__next'),
	responsive: [
		{
		  breakpoint: 992,
		  settings: {
			variableWidth: true,
			arrows: false,
			slidesToShow: 1,
		  }
		},
	]
});

// Cлайдер Licenses // 
$('.licenses__slider').slick({
	// centerMode: true,
	variableWidth: true,
	dots: false,
	infinite: true,
	speed: 300,
	slidesToShow: 1,
	arrows: true,
	prevArrow: $('.licenses__prev'),
    nextArrow: $('.licenses__next'),
});

// Cлайдер Reviews // 
$('.reviews__slider').slick({
	// centerMode: true,
	variableWidth: true,
	dots: false,
	infinite: true,
	speed: 300,
	slidesToShow: 1,
	arrows: true,
	prevArrow: $('.reviews__prev'),
    nextArrow: $('.reviews__next'),
});

// Navigation Dropdown // 

const dropDownButton = document.querySelectorAll(".menu__item");
const dropDownList = document.querySelectorAll(".dropdown__list");
const arrowDown = document.querySelector(".arrow-down");

dropDownButton.forEach(function (element) {
	element.addEventListener('click', function (event) {
		const $btn = event.target.closest('.menu__item');
		const id = $btn.dataset.id

		dropDownButton.forEach(btn => {
      		btn.classList.remove("dropdown__list--active");
    	});
    	
		$btn.classList.add("dropdown__list--active");
		
    	dropDownList.forEach(content => {
      		content.classList.remove("dropdown__list--active");
		});

    	const element = document.getElementById(id);
    	element.classList.add("dropdown__list--active");
	})
})

// ScrollUp //

$(function() {
    $('.scrollup').click(function() {
      $("html, body").animate({
        scrollTop:0
      },1000);
    })
  })
  $(window).scroll(function() {
    if ($(this).scrollTop()>200) {
      $('.scrollup').fadeIn();
    }
    else {
      $('.scrollup').fadeOut();
    }
});




